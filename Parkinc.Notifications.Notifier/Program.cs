﻿using System;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Loader;
using System.Threading;
using EasyNetQ;
using EasyNetQ.Consumer;
using Microsoft.Extensions.Configuration;
using Parkinc.Notifications.Notifier.Cache;
using StackExchange.Redis;

namespace Parkinc.Notifications.Notifier
{
    class Program
    {
        static void Main(string[] args)
        {
            var cfg = GetConfig();
            var redisConn = ConnectionMultiplexer.Connect(cfg["Redis:Host"]);
            var bus = RabbitHutch.CreateBus(cfg["Messaging:ConnectionString"]);

            var notifier = new Notifier(bus, new RedisCache(redisConn));
            notifier.Start(TimeSpan.FromMinutes(1));


            AssemblyLoadContext.Default.Unloading += context => notifier.Stop();
        }


        private static IConfiguration GetConfig()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            return builder.Build();
        }
    }
}
