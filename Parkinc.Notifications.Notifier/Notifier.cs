﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EasyNetQ;
using Parkinc.NotificationManager.DTOs;
using Parkinc.Notifications.Notifier.Cache;

namespace Parkinc.Notifications.Notifier
{
    public class Notifier
    {
        private readonly IBus _bus;
        private readonly IRedisCache _cache;

        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        public Notifier(IBus bus, IRedisCache cache)
        {
            _bus = bus;
            _cache = cache;
        }

        public async void Start(TimeSpan interval)
        {

            while (!_cts.Token.IsCancellationRequested)
            {
                var lastNotified = await _cache.Get(CacheKeys.NotifierLastNotified) ?? "00:00";
                var msg = new DateTimeDTO()
                {
                    DateTime = lastNotified,
                    Format = "HH:mm"
                };
                await _bus.PublishAsync(msg);
                Console.WriteLine($"Publishing last notified time {msg.DateTime} ({msg.Format})");
                await _cache.Set(CacheKeys.NotifierLastNotified, $"{DateTime.Now:HH:mm}");
                await Task.Delay(interval, _cts.Token);
            }
        }

        public void Stop()
        {
            _cts.Cancel();
        }
    }
}
