﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Parkinc.Notifications.Notifier.Cache
{
    public interface IRedisCache
    {
        Task Set(string key, string value, TimeSpan? expireIn = null);
        Task<string> Get(string key);
    }

    public class RedisCache : IRedisCache
    {
        private readonly IDatabase _db;
        public RedisCache(IConnectionMultiplexer multiplexer)
        {
            _db = multiplexer.GetDatabase();
        }

        public async Task Set(string key, string value, TimeSpan? expireIn = null)
        {
            var expiry = expireIn ?? TimeSpan.FromDays(7);
            await _db.StringSetAsync(key, value, expiry);
            Console.WriteLine($"Saved ({key}: {value}) to redis");
        }

        public async Task<string> Get(string key)
        {
            var value = await _db.StringGetAsync(key);
            if (!value.HasValue) return await Task.FromResult(default(string));
            Console.WriteLine($"Retrieved ({key}: {value}) from redis");
            return value;
        }
    }
}
