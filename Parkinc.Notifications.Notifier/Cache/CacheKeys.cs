﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.Notifications.Notifier.Cache
{
    public static class CacheKeys
    {
        public static string NotifierLastNotified => "NotifierLastNotified";
    }
}
